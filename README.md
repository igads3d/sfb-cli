[[_TOC_]]

# How to create a new meeting and get link for it

## Links

[Creating application](https://docs.microsoft.com/en-us/skype-sdk/ucwa/createanapplication)

[Authentication](https://docs.microsoft.com/en-us/skype-sdk/ucwa/authenticationinucwa)

[/myOnlineMeetings](https://docs.microsoft.com/en-us/skype-sdk/ucwa/myonlinemeetings_ref)


## Discovery

`xh GET https://lyncdiscover.<domain>`

``` json
{
    "_links": {
        "self": {
            "href": "https://webext.<domain>/Autodiscover/AutodiscoverService.svc/root?originalDomain=<domain>"
        },
        "user": {
            "href": "https://webext.<domain>/Autodiscover/AutodiscoverService.svc/root/oauth/user?originalDomain=<domain>"
        },
        "xframe": {
            "href": "https://webext.<domain>/Autodiscover/XFrame/XFrame.html"
        }
    }
}
```

A client must follow the link in `user` resource. Keep `user` and `xframe` links for later use.

## Authentication
### 401
Getting 401 is normal behaviour on first try as we are not yet authorized

`xh GET 'https://webext.<domain>/Autodiscover/AutodiscoverService.svc/root/oauth/user?originalDomain=<domain>'`

``` http
HTTP/2.0 401 Unauthorized
cache-control: no-cache
client-request-id: 691f536a-84e5-47b4-bac0-cd0c79be4f57
content-length: 1293
content-type: text/html
date: Thu, 21 Apr 2022 22:02:42 GMT
strict-transport-security: max-age=31536000; includeSubDomains
www-authenticate: Bearer trusted_issuers="00000002-0000-0ff1-ce00-000000000000@<domain>", client_id="00000004-0000-0ff1-ce00-000000000000"
www-authenticate: MsRtcOAuth href="https://webext.<domain>/WebTicket/oauthtoken",grant_type="urn:microsoft.rtc:windows,urn:microsoft.rtc:windows,urn:microsoft.rtc:anonmeeting,password"
x-content-type-options: nosniff
x-ms-correlation-id: 2147486017
x-ms-server-fqdn: <ms-server-fqdn>
x-powered-by: ARR/3.0

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
...
</html>
```

### OAuth

In headers section we can see the `www-authenticate` header. It contains `href` with OAuth URL and grant types:

`www-authenticate: MsRtcOAuth href="https://webext.<domain>/WebTicket/oauthtoken",grant_type="urn:microsoft.rtc:windows,urn:microsoft.rtc:windows,urn:microsoft.rtc:anonmeeting,password"`

for password auth you should provide `grant_type=password`, `username=<username>@<domain>` and `password=<password>` in form format: 

XH: `xh POST -v --form 'https://webext.<domain>/WebTicket/oauthtoken' grant_type=password username=<username>@<domain> password=<password>`

CURL: `curl -v -X POST 'https://webext.<domain>/WebTicket/oauthtoken' --data-urlencode grant_type=password --data-urlencode 'username=<username>@<domain>' --data-urlencode password=<password>` 

``` http
HTTP/2.0 200 OK
cache-control: no-store
client-request-id: 023fd4dd-3082-449b-b83e-80866116a91b
content-type: application/json
date: Thu, 21 Apr 2022 22:09:24 GMT
pragma: no-cache
strict-transport-security: max-age=31536000; includeSubDomains
vary: Accept-Encoding
x-content-type-options: nosniff
x-ms-correlation-id: 2147485232
x-ms-server-fqdn: <ms-server-fqdn>
x-powered-by: ARR/3.0

{
    "access_token": "cwt=AAEB...",
    "expires_in": 27589,
    "ms_rtc_identityscope": "local",
    "token_type": "Bearer"
}

```

`access token` is our bearer token, duh.

### Creating application

Now, let's try authenticating on `user` resource again:

XH: `xh --bearer '<access_token>' 'https://webext.<domain>/Autodiscover/AutodiscoverService.svc/root/oauth/user?originalDomain=<domain>'` 

CURL: `curl 'https://webext.<domain>/Autodiscover/AutodiscoverService.svc/root/oauth/user?originalDomain=<domain>' --oauth2-bearer <access_token>` 

``` http
HTTP/2.0 200 OK
cache-control: no-cache
client-request-id: c819d6be-a6cd-43e2-a8b0-a7d10490f442
content-type: application/json
date: Thu, 21 Apr 2022 22:20:48 GMT
expires: -1
pragma: no-cache
strict-transport-security: max-age=31536000; includeSubDomains
vary: Accept-Encoding
x-content-type-options: nosniff
x-ms-correlation-id: 2147491457
x-ms-server-fqdn: <ms-server-fqdn>
x-powered-by: ARR/3.0

{
    "_links": {
        "self": {
            "href": "https://webext.<domain>/Autodiscover/AutodiscoverService.svc/root/oauth/user"
        },
        "applications": {
            "href": "https://webext.<domain>/ucwa/oauth/v1/applications"
        },
        "xframe": {
            "href": "https://webext.<domain>/Autodiscover/XFrame/XFrame.html"
        }
    }
}
```

Finally, to register applicaton we need to POST to `applications` endpoint. Pass json with following parameters:

``` json
{
    "UserAgent": "UCWA Samples",
    "endpointId": "3038062d-df7c-4123-82e7-d05a8a4f6a37",
    "Culture": "en-US"
}
```

Where:

- `UserAgent` is self explanatory

- `endpointId` is UUID that you keep for accessing your application, should be unique among applications for the same user

- `Culture` is locale


XH: `xh POST --bearer '<access_token>' 'https://webext.<domain>/ucwa/oauth/v1/applications' UserAgent='UCWA Samples' endpointId=<UUID> Culture=en-US` 

CURL: `curl -X POST 'https://webext.<domain>/ucwa/oauth/v1/applications' --oauth2-bearer <access_token>  -H 'content-type: application/json' -H 'accept: application/json, */*;q=0.5' -d '{"UserAgent":"UCWA Samples","endpointId":"<UUID>","Culture":"en-US"}'`

And the server returns 201 Created:

``` http
HTTP/2.0 201 Created
cache-control: private
client-request-id: ceae8051-04e1-48a0-8f44-c2caab3532fc
client-request-id: 23813ccd-510c-4d9f-9b6b-babc6c832340
content-length: 4332
content-type: application/json; charset=utf-8
date: Thu, 21 Apr 2022 22:33:59 GMT
etag: "1803141906"
expires: Fri, 22 Apr 2022 06:34:08 GMT
p3p: CP="IDC CUR ADMa OUR BUS"
set-cookie: cwt_ucwa=AAEBHAEFAAAAAAAFFQAAAC-NRHC8J5iYyhKbKHh0AgCBEAzof_KnDIZStgGfIfSymb2CAtixgyDb_ePMhr9OKbai9LbCydnPvNS9jbIG61Wz-scN_ymO84YIMIIW2iMk2ggILS91Y3dhL29hdXRoL3YxL2FwcGxpY2F0aW9ucy8xMDg2Mjk1ODAxL3Bob3Rvcw; path=/ucwa/oauth/v1/applications/1086295801/photos; secure; HttpOnly
strict-transport-security: max-age=31536000; includeSubDomains
via: 1.1 <ms-server-fqdn> RtcExt
x-ms-correlation-id: 2147534147
x-ms-correlation-id: 2147485073
x-ms-server-fqdn: <ms-server-fqdn>
x-powered-by: ARR/3.0

{
    "culture": "en-US",
    "userAgent": "UCWA Samples",
    "type": "Browser",
    "_links": {
        "self": {
            "href": "/ucwa/oauth/v1/applications/1086295801"
        },
        "reportMyNetwork": {
            "href": "/ucwa/oauth/v1/applications/1086295801/reportMyNetwork"
        },
        "policies": {
            "href": "/ucwa/oauth/v1/applications/1086295801/policies"
        },
        "batch": {
            "href": "/ucwa/oauth/v1/applications/1086295801/batch"
        },
        "events": {
            "href": "/ucwa/oauth/v1/applications/1086295801/events?ack=1"
        }
    },
    "_embedded": {
        "me": {
            "uri": "sip:<username>@<domain>",
            "name": "Speps Spops",
            "emailAddresses": [
                "<username>@<domain>"
            ],
            "title": "Chief Retardation Officer",
            "department": "IT Dept",
            "company": "Acme",
            "_links": {
                "self": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/me"
                },
                "makeMeAvailable": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/me/makeMeAvailable",
                    "revision": "2"
                },
                "photo": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/photos/<username>@<domain>"
                }
            },
            "rel": "me"
        },
        "people": {
            "_links": {
                "self": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people"
                },
                "presenceSubscriptions": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/presenceSubscriptions"
                },
                "subscribedContacts": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/subscribedContacts"
                },
                "presenceSubscriptionMemberships": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/presenceSubscriptionMemberships"
                },
                "myGroups": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/groups",
                    "revision": "2"
                },
                "myGroupMemberships": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/groupMemberships",
                    "revision": "2"
                },
                "myContacts": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/contacts"
                },
                "myPrivacyRelationships": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/privacyRelationships"
                },
                "myContactsAndGroupsSubscription": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/contactsAndGroupsSubscription"
                },
                "search": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/search",
                    "revision": "2"
                }
            },
            "rel": "people"
        },
        "onlineMeetings": {
            "_links": {
                "self": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings"
                },
                "myOnlineMeetings": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/myOnlineMeetings"
                },
                "onlineMeetingDefaultValues": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/defaultValues"
                },
                "onlineMeetingEligibleValues": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/eligibleValues"
                },
                "onlineMeetingInvitationCustomization": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/customInvitation"
                },
                "onlineMeetingPolicies": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/policies"
                },
                "phoneDialInInformation": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/phoneDialInInformation"
                },
                "myAssignedOnlineMeeting": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/myOnlineMeetings/SNDL5LQ4"
                }
            },
            "rel": "onlineMeetings"
        },
        "communication": {
            "videoBasedScreenSharing": "Enabled",
            "1756f368-89f5-4608-af3f-8575ef00738f": "please pass this in a PUT request",
            "supportedModalities": [],
            "supportedMessageFormats": [
                "Plain"
            ],
            "audioPreference": "PhoneAudio",
            "conversationHistory": "Disabled",
            "publishEndpointLocation": true,
            "_links": {
                "self": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication"
                },
                "mediaRelayAccessToken": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication/mediaRelayAccessToken"
                },
                "mediaPolicies": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/mediaPolicies"
                },
                "conversations": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication/conversations?filter=active"
                },
                "startMessaging": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication/messagingInvitations",
                    "revision": "2"
                },
                "startAudioVideo": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication/audioVideoInvitations",
                    "revision": "2"
                },
                "startOnlineMeeting": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication/onlineMeetingInvitations?onlineMeetingUri=adhoc"
                },
                "joinOnlineMeeting": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication/onlineMeetingInvitations"
                },
                "missedItems": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication/missedItems"
                }
            },
            "rel": "communication",
            "etag": "3803157989"
        }
    },
    "rel": "application",
    "etag": "1803141906"
}
```

There are a lot of information about your user and a bunch of resources. For this particular usecase we will need:

- List and create our meetings: `.onlineMeetings._links.myOnlineMeetings` [LINK](https://docs.microsoft.com/en-us/skype-sdk/ucwa/myonlinemeetings_ref)

## Listing meetings

Using resource `.onlineMeetings._links.myOnlineMeetings` we get:

`xh GET --bearer '<access_token>' 'https://webext.<domain>/ucwa/oauth/v1/applications/1086295801/onlineMeetings/myOnlineMeetings'`

``` http
HTTP/2.0 200 OK
cache-control: no-cache
client-request-id: 161f9da1-b8ef-4a76-b195-5bfb50456e1d
client-request-id: 96b06b78-665d-4501-8c38-3fd2c80486cc
content-type: application/vnd.microsoft.com.ucwa+json; charset=utf-8
date: Thu, 21 Apr 2022 22:51:41 GMT
strict-transport-security: max-age=31536000; includeSubDomains
vary: Accept-Encoding
via: 1.1 <ms-server-fqdn> RtcExt
x-ms-correlation-id: 2147875866
x-ms-correlation-id: 2147485677
x-ms-server-fqdn: <ms-server-fqdn>
x-powered-by: ARR/3.0

{
    "_links": {
        "self": {
            "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/myOnlineMeetings"
        }
    },
    "_embedded": {
        "myAssignedOnlineMeeting": [
            {
                "onlineMeetingId": "SNDL5LQ4",
                "subject": "",
                "_links": {
                    "self": {
                        "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/myOnlineMeetings/SNDL5LQ4"
                    }
                },
                "rel": "myAssignedOnlineMeeting",
                "etag": "1647023354"
            }
        ],
        "myOnlineMeeting": [
            {
                "onlineMeetingId": "HLHDHMVR",
                "subject": "Subject 1",
                "_links": {
                    "self": {
                        "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/myOnlineMeetings/HLHDHMVR"
                    }
                },
                "rel": "myOnlineMeeting",
                "etag": "3486459850"
            },
            {
                "onlineMeetingId": "PHNPRK2C",
                "subject": "Bottom text",
                "_links": {
                    "self": {
                        "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/myOnlineMeetings/PHNPRK2C"
                    }
                },
                "rel": "myOnlineMeeting",
                "etag": "1749696971"
            },
            {
                "onlineMeetingId": "PN2WH6N9",
                "subject": "Demo demo scrub kaban",
                "_links": {
                    "self": {
                        "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/myOnlineMeetings/PN2WH6N9"
                    }
                },
                "rel": "myOnlineMeeting",
                "etag": "757139560"
            }
        ]
    },
    "rel": "myOnlineMeetings"
}
```

Simple enough, now let's create a new meeting.

## Creating meetings

to create a meeting we need to make a `POST` request to the very same `.../myOnlineMeetings` endpoint, but with a json:

```json
{
    "accessLevel": "Everyone",
    "entryExitAnnouncement": "Disabled",
    "attendees": [
        "sip:ses@sas.at", 
        "sip:spops@sas.at"
    ],
    "automaticLeaderAssignment": "Everyone",
    "description": "No description.",
    "leaders": [
        "sip:sas@sas.at"
    ],
    "phoneUserAdmission": "Disabled",
    "lobbyBypassForPhoneUsers": "Disabled",
    "subject": "Random subject",
}
```

it will result in returning extended json with a bit more fields. We only need `joinUrl` field, and this will be our join link. 

Yay! We did it!

But don't celebrate yet, because this link is still NOT in our calendar. Damn!

# Accessing Exchange 2010 on-premise

The pain continues.

## Links

[Autodiscovery](https://docs.microsoft.com/en-us/exchange/client-developer/exchange-web-services/autodiscover-for-exchange)

[Authentication](https://docs.microsoft.com/en-us/exchange/client-developer/exchange-web-services/authentication-and-ews-in-exchange)

[Nylas exchangelib](https://github.com/nylas/exchangelib)

## Autodiscovery

As for SfB, first step here is autodiscovery too. And this is where problems begin. _Where\_is\_that\_autodiscovery?.jpg_.  The answer is: in a few places, they are described at the [documentation page](https://docs.microsoft.com/en-us/exchange/client-developer/exchange-web-services/autodiscover-for-exchange#phase-1-defining-the-candidate-pool). Try some combinations, try with your web browser, try with basic auth too, and you may discover the truth.

As for me, i found mine at `https://autodiscover.<domain>/autodiscover/autodiscover.svc` behind basic auth. Sadly, XML endpoint didn't work.

XH: `xh --follow --auth <email>:<password> 'https://autodiscover.<domain>/autodiscover/autodiscover.svc'`

``` http
HTTP/1.1 200 OK
Accept-Ranges: bytes
Cache-Control: private
Content-Type: text/xml
Date: Fri, 13 May 2022 21:24:20 GMT
Etag: "80604dcc4fd71:0"
Last-Modified: Wed, 03 Mar 2021 00:33:41 GMT
Request-Id: e3f3fc08-c9c4-49c9-8ecc-d6746f07c2ba
Server: Microsoft-IIS/10.0
Set-Cookie: X-BackEndCookie=S-1-5-21-1883540783-2560108476-681251530-160888=u56Lnp2ejJqBz82Zx8/Mz8/Sxs3IxtLLy8uc0p6Zz83SzMrNzsqaxpudy5qagYHNz83N0s/J0s7Nq83Oxc3Lxc3OgYmajIue0Y2Kgc8=; expires=Sun, 12-Jun-2022 21:24:21 GMT; path=/autodiscover; secure; HttpOnly
Set-Cookie: NSC_wtsw_Fydibohf2016_bvupejtdpwfs=ffffffff09681caa45525d5f4f58455e445a4a42378b;expires=Fri, 13-May-2022 21:25:24 GMT;path=/;secure;httponly
Vary: Accept-Encoding
X-Aspnet-Version: 4.0.30319
X-Beserver: \*\*\*
X-Calculatedbetarget: \*\*\*
X-Diaginfo: \*\*\*
X-Feserver: \*\*\*
X-Powered-By: ASP.NET

<?xml version="1.0" encoding="utf-8"?>
<wsdl:definitions xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:wsam="http://www.w3.org/2007/05/addressing/metadata" xmlns:tns="http://schemas.microsoft.com/exchange/2010/Autodiscover" xmlns:wsa="http://schemas.xmlsoap.org/ws/2004/08/addressing" xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy" xmlns:wsap="http://schemas.xmlsoap.org/ws/2004/08/addressing/policy" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:wsa10="http://www.w3.org/2005/08/addressing" xmlns:wsx="http://schemas.xmlsoap.org/ws/2004/09/mex" targetNamespace="http://schemas.microsoft.com/exchange/2010/Autodiscover" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
  <wsdl:types>
    <xsd:schema targetNamespace="http://schemas.microsoft.com/exchange/2010/Autodiscover/Imports">
      <xsd:import namespace="http://schemas.microsoft.com/exchange/2010/Autodiscover" location="messages.xsd" />
    </xsd:schema>
  </wsdl:types>
  <wsdl:message name="GetUserSettingsRequestMessage">
    <wsdl:part name="parameters" element="tns:GetUserSettingsRequestMessage" />
  </wsdl:message>
  <wsdl:message name="GetUserSettingsRequestMessage_Headers">
    <wsdl:part name="RequestedServerVersion" element="tns:RequestedServerVersion" />
  </wsdl:message>
  <wsdl:message name="GetUserSettingsResponseMessage">
    <wsdl:part name="parameters" element="tns:GetUserSettingsResponseMessage" />
  </wsdl:message>
  <wsdl:message name="GetUserSettingsResponseMessage_Headers">
    <wsdl:part name="ServerVersionInfo" element="tns:ServerVersionInfo" />
  </wsdl:message>
  <wsdl:message name="GetDomainSettingsRequestMessage">
    <wsdl:part name="parameters" element="tns:GetDomainSettingsRequestMessage" />
  </wsdl:message>
  <wsdl:message name="GetDomainSettingsRequestMessage_Headers">
    <wsdl:part name="RequestedServerVersion" element="tns:RequestedServerVersion" />
  </wsdl:message>
  <wsdl:message name="GetDomainSettingsResponseMessage">
    <wsdl:part name="parameters" element="tns:GetDomainSettingsResponseMessage" />
  </wsdl:message>
  <wsdl:message name="GetDomainSettingsResponseMessage_Headers">
    <wsdl:part name="ServerVersionInfo" element="tns:ServerVersionInfo" />
  </wsdl:message>
  <wsdl:message name="GetFederationInformationRequestMessage">
    <wsdl:part name="parameters" element="tns:GetFederationInformationRequestMessage" />
  </wsdl:message>
  <wsdl:message name="GetFederationInformationRequestMessage_Headers">
    <wsdl:part name="RequestedServerVersion" element="tns:RequestedServerVersion" />
  </wsdl:message>
  <wsdl:message name="GetFederationInformationResponseMessage">
    <wsdl:part name="parameters" element="tns:GetFederationInformationResponseMessage" />
  </wsdl:message>
  <wsdl:message name="GetFederationInformationResponseMessage_Headers">
    <wsdl:part name="ServerVersionInfo" element="tns:ServerVersionInfo" />
  </wsdl:message>
  <wsdl:message name="GetOrganizationRelationshipSettingsRequestMessage">
    <wsdl:part name="parameters" element="tns:GetOrganizationRelationshipSettingsRequestMessage" />
  </wsdl:message>
  <wsdl:message name="GetOrganizationRelationshipSettingsRequestMessage_Headers">
    <wsdl:part name="RequestedServerVersion" element="tns:RequestedServerVersion" />
  </wsdl:message>
  <wsdl:message name="GetOrganizationRelationshipSettingsResponseMessage">
    <wsdl:part name="parameters" element="tns:GetOrganizationRelationshipSettingsResponseMessage" />
  </wsdl:message>
  <wsdl:message name="GetOrganizationRelationshipSettingsResponseMessage_Headers">
    <wsdl:part name="ServerVersionInfo" element="tns:ServerVersionInfo" />
  </wsdl:message>
  <wsdl:portType name="Autodiscover">
    <wsdl:operation name="GetUserSettings">
      <wsdl:input wsaw:Action="http://schemas.microsoft.com/exchange/2010/Autodiscover/Autodiscover/GetUserSettings" name="GetUserSettingsRequestMessage" message="tns:GetUserSettingsRequestMessage" />
      <wsdl:output wsaw:Action="http://schemas.microsoft.com/exchange/2010/Autodiscover/Autodiscover/GetUserSettingsResponse" name="GetUserSettingsResponseMessage" message="tns:GetUserSettingsResponseMessage" />
    </wsdl:operation>
    <wsdl:operation name="GetDomainSettings">
      <wsdl:input wsaw:Action="http://schemas.microsoft.com/exchange/2010/Autodiscover/Autodiscover/GetDomainSettings" name="GetDomainSettingsRequestMessage" message="tns:GetDomainSettingsRequestMessage" />
      <wsdl:output wsaw:Action="http://schemas.microsoft.com/exchange/2010/Autodiscover/Autodiscover/GetDomainSettingsResponse" name="GetDomainSettingsResponseMessage" message="tns:GetDomainSettingsResponseMessage" />
    </wsdl:operation>
    <wsdl:operation name="GetFederationInformation">
      <wsdl:input wsaw:Action="http://schemas.microsoft.com/exchange/2010/Autodiscover/Autodiscover/GetFederationInformation" name="GetFederationInformationRequestMessage" message="tns:GetFederationInformationRequestMessage" />
      <wsdl:output wsaw:Action="http://schemas.microsoft.com/exchange/2010/Autodiscover/Autodiscover/GetFederationInformationResponse" name="GetFederationInformationResponseMessage" message="tns:GetFederationInformationResponseMessage" />
    </wsdl:operation>
    <wsdl:operation name="GetOrganizationRelationshipSettings">
      <wsdl:input wsaw:Action="http://schemas.microsoft.com/exchange/2010/Autodiscover/Autodiscover/GetOrganizationRelationshipSettings" name="GetOrganizationRelationshipSettingsRequestMessage" message="tns:GetOrganizationRelationshipSettingsRequestMessage" />
      <wsdl:output wsaw:Action="http://schemas.microsoft.com/exchange/2010/Autodiscover/Autodiscover/GetOrganizationRelationshipSettingsResponse" name="GetOrganizationRelationshipSettingsResponseMessage" message="tns:GetOrganizationRelationshipSettingsResponseMessage" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="DefaultBinding_Autodiscover" type="tns:Autodiscover">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="GetUserSettings">
      <soap:operation soapAction="http://schemas.microsoft.com/exchange/2010/Autodiscover/Autodiscover/GetUserSettings" style="document" />
      <wsdl:input name="GetUserSettingsRequestMessage">
        <soap:header message="tns:GetUserSettingsRequestMessage_Headers" part="RequestedServerVersion" use="literal" />
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output name="GetUserSettingsResponseMessage">
        <soap:header message="tns:GetUserSettingsResponseMessage_Headers" part="ServerVersionInfo" use="literal" />
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetDomainSettings">
      <soap:operation soapAction="http://schemas.microsoft.com/exchange/2010/Autodiscover/Autodiscover/GetDomainSettings" style="document" />
      <wsdl:input name="GetDomainSettingsRequestMessage">
        <soap:header message="tns:GetDomainSettingsRequestMessage_Headers" part="RequestedServerVersion" use="literal" />
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output name="GetDomainSettingsResponseMessage">
        <soap:header message="tns:GetDomainSettingsResponseMessage_Headers" part="ServerVersionInfo" use="literal" />
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetFederationInformation">
      <soap:operation soapAction="http://schemas.microsoft.com/exchange/2010/Autodiscover/Autodiscover/GetFederationInformation" style="document" />
      <wsdl:input name="GetFederationInformationRequestMessage">
        <soap:header message="tns:GetFederationInformationRequestMessage_Headers" part="RequestedServerVersion" use="literal" />
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output name="GetFederationInformationResponseMessage">
        <soap:header message="tns:GetFederationInformationResponseMessage_Headers" part="ServerVersionInfo" use="literal" />
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetOrganizationRelationshipSettings">
      <soap:operation soapAction="http://schemas.microsoft.com/exchange/2010/Autodiscover/Autodiscover/GetOrganizationRelationshipSettings" style="document" />
      <wsdl:input name="GetOrganizationRelationshipSettingsRequestMessage">
        <soap:header message="tns:GetOrganizationRelationshipSettingsRequestMessage_Headers" part="RequestedServerVersion" use="literal" />
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output name="GetOrganizationRelationshipSettingsResponseMessage">
        <soap:header message="tns:GetOrganizationRelationshipSettingsResponseMessage_Headers" part="ServerVersionInfo" use="literal" />
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
</wsdl:definitions>
``` 

What to do next? IDK, i need to learn some more about SOAP and shit. And finish my SfB "integration".

WIP, probably. Or not, because i can just use damn web version. 
