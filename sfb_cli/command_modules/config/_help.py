from knack.help_files import helps

helps['config'] = """
type: group
short-summary: Manage user configuration
"""

helps['config show'] = """
type: command
short-summary: Show, which account you are authenticated with
"""
