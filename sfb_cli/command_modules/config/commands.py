from knack.commands import CommandGroup
# from knack.arguments import ArgumentsContext

import command_modules.config._help

def load_group(loader):
    with CommandGroup(loader, 'config', 'command_modules.config.custom#{}', is_preview=True) as g:
        g.command('show', 'config_show_handler')
