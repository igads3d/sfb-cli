import core.consts as consts

from knack.log import get_logger
logger = get_logger(__name__)

def config_show_handler():
    global AUTH_CONTEXT
    logger.warning('Configured for %s', consts.AUTH_CONTEXT.email) 
