from knack.help_files import helps

helps['init'] = """
type: command
short-summary: Set up email and password for further use
parameters:
  - name: --email
  - name: --password
"""
