from knack.commands import CommandGroup
from knack.arguments import ArgumentsContext

import command_modules.init._help

def load_group(loader):
    with CommandGroup(loader, '', 'command_modules.init.custom#{}') as g:
        g.command('init', 'init_command_handler', is_preview=False)

def load_args(loader):
    with ArgumentsContext(loader, 'init') as ac:
        ac.argument('email', type=str)
        ac.argument('password', type=str)
