import uuid

from core.AuthContext import AuthContext
from knack.log import get_logger


logger = get_logger(__name__)

def init_command_handler(email: str='', password: str=''):
    context = AuthContext()
    if email != '':
        context.set_email(email)
    else:
        from knack.prompting import prompt
        context.set_email(prompt('Enter your work email: '))

    if password != '':
        context.set_pass(password)
    else:
        from knack.prompting import prompt_pass
        context.set_pass(prompt_pass(confirm=True))

    context.app_id = str(uuid.uuid5(uuid.NAMESPACE_URL, context.domain + '/' + context.user))
    context.save()
    logger.warning('User config %s saved successfully.', context.email)
    return
