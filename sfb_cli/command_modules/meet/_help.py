from knack.help_files import helps

helps['meet'] = """
type: group
short-summary: Manage meetings
"""

helps['meet show'] = """
type: command
short-summary: Show current meetings
"""

helps['meet add'] = """
type: command
short-summary: Add new meeting
parameters:
    - name: subject
      type: str
      short-summary: Add a subject to your meeting
    - name: leaders
      type: str
      short-summary: Explicitly define meeting leaders
examples:
    - name: Add standard meeting
      text: >
        'sfb-cli meet add --subject "Example subject" --attendees sip:sos@ses.com,sip:sas@ses.com'
"""

