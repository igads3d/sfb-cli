from collections import _OrderedDictItemsView, OrderedDict
import json

from knack.log import get_logger
logger = get_logger(__name__)

def _transform_dict_entry_to_row(entry: dict, whitelist: list):
    row = OrderedDict()

    for key, value in entry.items():
        logger.debug('%s: %s', key, value)
        if key in whitelist:
            row[key] = value

    return row

def meet_show_table_transform(input):
    table = []
    unpacked = input['_embedded']
    for meeting in unpacked['myOnlineMeeting']:
        table.append(_transform_dict_entry_to_row(meeting,
                                         ['onlineMeetingId',
                                          'rel',
                                          'subject']))

    for meeting in unpacked['myAssignedOnlineMeeting']:
        table.append(_transform_dict_entry_to_row(meeting,
                                         ['onlineMeetingId',
                                          'rel',
                                          'subject']))
    logger.debug(table)
    return table


def _test_meet_show_table_transform(input):
    row1 = OrderedDict()
    row2 = OrderedDict()
    row1['col1'] = 'asd'
    row1['col2'] = 'zxc'

    row2['col1'] = 'asd1'
    row2['col2'] = 'zxc1'
    return [row1, row2]
