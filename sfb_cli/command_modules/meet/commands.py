from knack.commands import CommandGroup
from knack.arguments import ArgumentsContext

from command_modules.meet._completers import email_completer
from command_modules.meet._transformers import meet_show_table_transform
import command_modules.meet._help

def load_group(loader):
    with CommandGroup(loader, 'meet', 'command_modules.meet.custom#{}', is_preview=True) as g:
        g.command('add', 'meet_add_command_handler')
        g.command('show', 'meet_show_command_handler', table_transformer=meet_show_table_transform)

def load_args(loader):
    with ArgumentsContext(loader, 'meet add') as ac:
        ac.argument('leaders', completer=email_completer)
        ac.argument('subject', type=str)
