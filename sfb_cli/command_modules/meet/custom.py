import core.consts as consts
from core.RequestManager import RequestManager

from knack.log import get_logger

logger = get_logger(__name__)


def meet_add_command_handler(subject: str='Quick meet', leaders: str=''):
    context = consts.AUTH_CONTEXT
    rm = RequestManager(context, verify_local=False)
    rm.get_discovery_resources(context)
    rm.get_user_resource_or_oauth_link(context)
    rm._get_application(context)

    payload = {}
    payload['accessLevel'] = 'SameEnterprise'
    # payload['attendees'] = leaders.split(',')  # doesn't work when leader == attendee 
    payload['automaticLeaderAssignment'] = 'Everyone'
    payload['description'] = 'No description.'
    payload['leaders'] = leaders.split(',') if leaders != '' else ''
    payload['lobbyBypassForPhoneUsers'] = 'Disabled'
    payload['phoneUserAdmission'] = 'Disabled'
    payload['subject'] = subject


    logger.debug('Payload:\n%s', payload)
    return rm.meetings.create_meeting(rm, context, payload)


def meet_show_command_handler():
    context = consts.AUTH_CONTEXT
    rm = RequestManager(context, verify_local=False)
    rm.get_discovery_resources(context)
    rm.get_user_resource_or_oauth_link(context)
    rm._get_application(context)
    return rm.meetings.get_meetings(rm, context)
