from knack.help_files import helps

helps['test'] = """
type: group
short-summary: Test commands to aid in the development.
"""

helps['test auth'] = """
type: command
short-summary: Authenticate, get token and load resources from the answer
"""

helps['test discovery'] = """
type: command
short-summary: Test discovery endpoint and load resources. Works from local and global networks
"""

helps['test json'] = """
type: command
short-summary: Test loading big jsons to resources cache
"""

helps['test saveload'] = """
type: command
short-summary: Test saving and loading user profile.
long-summary: |
    Save and load predefined dummy profile.
    Overwrites your current profile, so you will have to 'init' again.
"""
