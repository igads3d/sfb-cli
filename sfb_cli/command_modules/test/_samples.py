sample_json = {
    "culture": "en-US",
    "userAgent": "UCWA Samples",
    "type": "Browser",
    "_links": {
        "self": {
            "href": "/ucwa/oauth/v1/applications/1086295801"
        },
        "reportMyNetwork": {
            "href": "/ucwa/oauth/v1/applications/1086295801/reportMyNetwork"
        },
        "policies": {
            "href": "/ucwa/oauth/v1/applications/1086295801/policies"
        },
        "batch": {
            "href": "/ucwa/oauth/v1/applications/1086295801/batch"
        },
        "events": {
            "href": "/ucwa/oauth/v1/applications/1086295801/events?ack=1"
        }
    },
    "_embedded": {
        "me": {
            "uri": "sip:<username>@<domain>",
            "name": "Speps Spops",
            "emailAddresses": [
                "<username>@<domain>"
            ],
            "title": "Chief Retardation Officer",
            "department": "IT Dept",
            "company": "Acme",
            "_links": {
                "self": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/me"
                },
                "makeMeAvailable": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/me/makeMeAvailable",
                    "revision": "2"
                },
                "photo": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/photos/<username>@<domain>"
                }
            },
            "rel": "me"
        },
        "people": {
            "_links": {
                "self": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people"
                },
                "presenceSubscriptions": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/presenceSubscriptions"
                },
                "subscribedContacts": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/subscribedContacts"
                },
                "presenceSubscriptionMemberships": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/presenceSubscriptionMemberships"
                },
                "myGroups": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/groups",
                    "revision": "2"
                },
                "myGroupMemberships": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/groupMemberships",
                    "revision": "2"
                },
                "myContacts": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/contacts"
                },
                "myPrivacyRelationships": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/privacyRelationships"
                },
                "myContactsAndGroupsSubscription": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/contactsAndGroupsSubscription"
                },
                "search": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/people/search",
                    "revision": "2"
                }
            },
            "rel": "people"
        },
        "onlineMeetings": {
            "_links": {
                "self": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings"
                },
                "myOnlineMeetings": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/myOnlineMeetings"
                },
                "onlineMeetingDefaultValues": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/defaultValues"
                },
                "onlineMeetingEligibleValues": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/eligibleValues"
                },
                "onlineMeetingInvitationCustomization": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/customInvitation"
                },
                "onlineMeetingPolicies": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/policies"
                },
                "phoneDialInInformation": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/phoneDialInInformation"
                },
                "myAssignedOnlineMeeting": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/onlineMeetings/myOnlineMeetings/SNDL5LQ4"
                }
            },
            "rel": "onlineMeetings"
        },
        "communication": {
            "videoBasedScreenSharing": "Enabled",
            "1756f368-89f5-4608-af3f-8575ef00738f": "please pass this in a PUT request",
            "supportedModalities": [],
            "supportedMessageFormats": [
                "Plain"
            ],
            "audioPreference": "PhoneAudio",
            "conversationHistory": "Disabled",
            "publishEndpointLocation": True,
            "_links": {
                "self": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication"
                },
                "mediaRelayAccessToken": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication/mediaRelayAccessToken"
                },
                "mediaPolicies": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/mediaPolicies"
                },
                "conversations": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication/conversations?filter=active"
                },
                "startMessaging": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication/messagingInvitations",
                    "revision": "2"
                },
                "startAudioVideo": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication/audioVideoInvitations",
                    "revision": "2"
                },
                "startOnlineMeeting": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication/onlineMeetingInvitations?onlineMeetingUri=adhoc"
                },
                "joinOnlineMeeting": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication/onlineMeetingInvitations"
                },
                "missedItems": {
                    "href": "/ucwa/oauth/v1/applications/1086295801/communication/missedItems"
                }
            },
            "rel": "communication",
            "etag": "3803157989"
        }
    },
    "rel": "application",
    "etag": "1803141906"}
