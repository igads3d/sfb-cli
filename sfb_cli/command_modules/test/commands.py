from knack.commands import CommandGroup

import command_modules.test._help

def load_group(loader):
    with CommandGroup(loader, 'test', 'command_modules.test.custom#{}', is_experimental=True) as g:
        g.command('discovery', 'test_discovery_handler')
        g.command('auth', 'test_auth_handler')
        g.command('json', 'test_json_load_handler')
        g.command('saveload', 'test_saveload_handler', confirmation=True)
        g.command('getapp', 'test_getapp_handler')

#def load_args(loader):
#    with ArgumentsContext(loader, 'test saveload') as ac:
#        ac.argument('--test-arg', type=str )
