import core.consts as consts
import command_modules.test._samples as samples

from core.RequestManager import RequestManager
from core.AuthContext import AuthContext
from core.AuthManager import AuthManager as am

from knack.log import get_logger
logger = get_logger(__name__)


def test_discovery_handler():
    context = consts.AUTH_CONTEXT
    rm = RequestManager(context, verify_local=False)
    rm.get_discovery_resources(context)


def test_auth_handler():
    context = consts.AUTH_CONTEXT
    rm = RequestManager(context, verify_local=False)
    rm.get_discovery_resources(context)
    logger.info(rm.get_user_resource_or_oauth_link(context))


def test_saveload_handler():
    context = consts.AUTH_CONTEXT
    context.set_email('test@domain.com')
    context.set_pass('testpass')
    context.save()
    new_context = AuthContext()
    new_context.load()
    logger.warning('Loaded with data: %s', new_context)


def test_getapp_handler():
    context = consts.AUTH_CONTEXT
    rm = RequestManager(context, verify_local=False)
    rm.get_discovery_resources(context)
    rm.get_user_resource_or_oauth_link(context)
    rm._get_application(context)


def test_json_load_handler():
    context = consts.AUTH_CONTEXT
    rm = RequestManager(context)
    sample_json = samples.sample_json

    context.load_links_into_resources(sample_json)
    for key, value in context.resources.items():
        logger.info(key + ': ' + str(value))
