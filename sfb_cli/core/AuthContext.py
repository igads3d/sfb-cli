from dataclasses import dataclass, fields
from typing import Any
from pathlib import Path
import uuid
import toml
from knack.log import get_logger


logger = get_logger(__name__)

@dataclass
class AuthContext:
    domain: str
    user: str
    email: str
    passwd: str
    access_token: str
    config_file: str
    app_id: str
    resources = {}
    save_fields = ['email',
                   'passwd',
                   'app_id']

    def __init__(self, config_file: str=str(Path(Path.home()) / '.sfb-cli.py' / 'user-config.toml')):
        self.config_file = config_file
        self.__post_init()
        return

    def __post_init(self):
        for field in fields(self):
            try:
                getattr(self, field.name)
            except AttributeError:
                setattr(self, field.name, None)

    def set_email(self, email: str):
        self.email = email
        self.user = email.split('@')[0]
        self.domain = email.split('@')[1]

    def set_pass(self, passwd: str=''):
        if passwd != '':
            self.passwd = passwd
            return
        raise NotImplementedError('Passwordless auth is not implemented yet')

    def set_resource(self, key: str, value: str):
        self.resources[key] = value
        logger.debug('Resource \'%s\' is set to \'%s\'', key, value)

    def load_links_into_resources(self, response: Any):
        from knack.log import get_logger
        logger = get_logger(__name__)

        if type(response) != dict:
            logger.debug('%s is string. Ignoring.', str(response))
            return
        for key, value in response.items():
            try:
                if value.get('href') is not None:
                    self.set_resource(key, value.get('href'))
                else:
                    logger.debug('\'%s\' has no links, stepping inside.', key)
                    self.load_links_into_resources(value)
            except AttributeError:
                logger.debug('\'%s\': \'%s\' value is string. Ignoring.', key, value)
                continue

    def add_host_to_resources(self, host: str):
        for key, link in self.resources.items():
            if link.startswith('/'):
                self.resources[key] = str(host + link)
                logger.debug('Added host to resource: %s',
                             self.resources[key])

    def as_dict_for_save(self):
        result = {}
        for field in fields(self):
            if field.name in self.save_fields:
                result[field.name] = getattr(self, field.name)
            continue
        return result

    def save(self):
        logger.debug('Writing config to file %s', self.config_file)
        with open(self.config_file, 'w') as f:
            tomldata = toml.dumps(self.as_dict_for_save())
            logger.debug(tomldata)
            f.write(tomldata)

    def load(self):
        logger.debug('Loading from config file %s', self.config_file)
        tomldata = toml.load(self.config_file)
        for key, value in tomldata.items():
            if key == 'passwd':
                self.set_pass(value)
            elif key == 'email':
                self.set_email(value)
            else:
                setattr(self, key, value)
        logger.debug('Loaded context: %s', self.__str__())

