import requests as rq
from typing import Callable, Iterable

from core.AuthContext import AuthContext
from core.exceptions import AuthenticationError

class AuthManager:
    def __init__(self):
        pass

    @staticmethod
    def _form_bearer_token(auth_context: AuthContext):
        return 'Bearer ' + auth_context.access_token

    @staticmethod
    def make_auth_request(auth_context: AuthContext, method: Callable, url: str, data: Iterable={}):
        from knack.log import get_logger
        logger = get_logger(__name__)

        if data == {}:
            logger.debug('Authenticated request w/o json')
            return method(url, headers={'Authorization': AuthManager._form_bearer_token(auth_context)})
        return method(url, headers={'Authorization': AuthManager._form_bearer_token(auth_context)}, json=data)

    @staticmethod
    def get_access_token(auth_context: AuthContext):
        from knack.log import get_logger
        logger = get_logger(__name__)

        # This is done to mimic form-data
        data = {'grant_type': 'password',
                 'username': auth_context.email,
                 'password': auth_context.passwd}
        response = rq.post(auth_context.resources.get('OAUTH'), data=data, ).json()

        if response.get('error') is not None:
            raise AuthenticationError(response['error'])

        auth_context.access_token = response.get('access_token')
        logger.info('Got token: %s', auth_context.access_token[:10] + '...')
