import requests as rq

from core.AuthContext import AuthContext
#from core.RequestManager import RequestManager
from core.AuthManager import AuthManager as am

class OnlineMeetings:

    def __init__(self):
        pass


    def get_meetings(self, rm, context: AuthContext):
        response = am.make_auth_request(context,
                                        rq.get,
                                        context.resources['myOnlineMeetings'])
        return response.json()


    def create_meeting(self, rm, context: AuthContext, payload: dict):
        response = am.make_auth_request(context,
                                        rq.post,
                                        context.resources['myOnlineMeetings'],
                                        payload)
        return response.json()
