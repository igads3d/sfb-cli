from typing import Any
import requests as rq

from core.AuthContext import AuthContext
from core.AuthManager import AuthManager
from core.OnlineMeetings import OnlineMeetings


class RequestManager:
    discovery_url = 'https://lyncdiscover.'
    discovery_url_local = 'https://lyncdiscoverinternal.'
    verify_global = None
    verify_local = None
    meetings: OnlineMeetings

    def __init__(self, auth_context: AuthContext, verify_global: bool=True, verify_local: bool=True):
        from knack.log import get_logger
        logger = get_logger(__name__)

        self.discovery_url += auth_context.domain
        self.discovery_url_local += auth_context.domain
        self.verify_local = verify_local
        self.verify_global = verify_global

        self.meetings = OnlineMeetings()

        logger.debug('Set global Discovery URL: %s, verification %s',
                     self.discovery_url, self.verify_global)
        logger.debug('Set local Discovery URL: %s, verification %s',
                     self.discovery_url_local, self.verify_local)


    def get_discovery_resources(self, auth_context: AuthContext):
        from knack.log import get_logger
        logger = get_logger(__name__)
        response = None

        try:
            logger.info('Trying global Discovery URL...')
            response = rq.get(self.discovery_url, verify=self.verify_global).json()
        except rq.ConnectionError:
            logger.warning('Cannot connect to global Discovery URL')
            logger.info('Trying local Discovery URL...')
            response = rq.get(self.discovery_url_local, verify=self.verify_local).json()
        logger.debug('Discovery returned %s', response)

        auth_context.load_links_into_resources(response)
        logger.info('Discovery done')
        logger.debug('Resources after discovery: %s', auth_context.resources)
        return response

    def get_user_resource_or_oauth_link(self, auth_context: AuthContext):
        import re
        from knack.log import get_logger
        logger = get_logger(__name__)

        logger.info('Trying user resource...')
        url = auth_context.resources.get('user')
        response = rq.get(url)
        if response.status_code != 200:
            header = response.headers.get('WWW-Authenticate')
            oauth_link = re.findall(r'href=\S*', header)[0].split(',')[0].split('=')[1][1:-1]
            auth_context.set_resource('OAUTH', oauth_link)
            AuthManager.get_access_token(auth_context)
            response = AuthManager.make_auth_request(auth_context, rq.get, url)
            auth_context.load_links_into_resources(response.json())
            logger.info('User resource done')
        return response.json()

    def _get_application(self, auth_context: AuthContext):
        from knack.log import get_logger
        logger = get_logger(__name__)

        logger.info('Getting application...')
        payload = {'UserAgent': 'UCWA Samples',
                   'endpointId': auth_context.app_id,
                   'Culture': 'en-US'}
        logger.debug('Formed payload: %s', payload)

        response = AuthManager.make_auth_request(auth_context,
                                       rq.post,
                                       auth_context.resources['applications'],
                                       payload)

        link_split = auth_context.resources['applications'].split('/')
        proto = link_split[0] + '//'
        domain = link_split[2]
        host = proto + domain

        auth_context.load_links_into_resources(response.json())
        auth_context.add_host_to_resources(host)
        logger.info('Getting application done')
        logger.debug('POST /applications returned: %s', response)

