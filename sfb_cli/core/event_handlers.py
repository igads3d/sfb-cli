from knack.log import get_logger

from core.AuthContext import AuthContext
import core.consts as consts

logger = get_logger(__name__)

def pre_execute_load_config_handler(cli, **kwargs):
    consts.AUTH_CONTEXT = AuthContext()
    try:
        consts.AUTH_CONTEXT.load()
    except:
        logger.warn('No user-config file found. Consider running \'init\' command.')
