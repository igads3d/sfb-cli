#!/usr/bin/env python

""" User registers commands with CommandGroups """

import os
import sys

from knack import CLI
from knack.commands import CLICommandsLoader
from knack.events import EVENT_CLI_PRE_EXECUTE
from knack.help import CLIHelp

import core.event_handlers as eh
import command_modules.test.commands as test_cmd
import command_modules.meet.commands as meet_cmd
import command_modules.init.commands as init_cmd
import command_modules.config.commands as config_cmd

from core.AuthContext import AuthContext
from core.AuthManager import AuthManager
from core.RequestManager import RequestManager


cli_name = os.path.basename(__file__)


CONFIG_DIR = os.path.expanduser(os.path.join('~', '.{}'.format(cli_name)))
WELCOME_MESSAGE = r"""
  ______   __                                                                          __
 /      \ |  \                                                                        |  \
|  $$$$$$\| $$   __  __    __   ______    ______          _______  __    __   _______ | $$   __   _______
| $$___\$$| $$  /  \|  \  |  \ /      \  /      \        /       \|  \  |  \ /       \| $$  /  \ /       \
 \$$    \ | $$_/  $$| $$  | $$|  $$$$$$\|  $$$$$$\      |  $$$$$$$| $$  | $$|  $$$$$$$| $$_/  $$|  $$$$$$$
 _\$$$$$$\| $$   $$ | $$  | $$| $$  | $$| $$    $$       \$$    \ | $$  | $$| $$      | $$   $$  \$$    \
|  \__| $$| $$$$$$\ | $$__/ $$| $$__/ $$| $$$$$$$$       _\$$$$$$\| $$__/ $$| $$_____ | $$$$$$\  _\$$$$$$\
 \$$    $$| $$  \$$\ \$$    $$| $$    $$ \$$     \      |       $$ \$$    $$ \$$     \| $$  \$$\|       $$
  \$$$$$$  \$$   \$$ _\$$$$$$$| $$$$$$$   \$$$$$$$       \$$$$$$$   \$$$$$$   \$$$$$$$ \$$   \$$ \$$$$$$$
                    |  \__| $$| $$
                     \$$    $$| $$
                      \$$$$$$  \$$


Welcome to Skype for Business CLI (UNOFFICIAL!)
"""


class SfbCLIHelp(CLIHelp):

    def __init__(self, cli_ctx=None):
        super(SfbCLIHelp, self).__init__(cli_ctx=cli_ctx,
                                        privacy_statement='Something something privacy',
                                        welcome_message=WELCOME_MESSAGE)


class SfbCLICommandsLoader(CLICommandsLoader):

    def load_command_table(self, args):
        init_cmd.load_group(self)
        meet_cmd.load_group(self)
        test_cmd.load_group(self)
        config_cmd.load_group(self)

        return super(SfbCLICommandsLoader, self).load_command_table(args)


    def load_arguments(self, command):
        init_cmd.load_args(self)
        meet_cmd.load_args(self)

        super(SfbCLICommandsLoader, self).load_arguments(command)


class SfbCLI(CLI):
    auth_context = None
    def set_auth_context(self, auth_context: AuthContext):
        self.auth_context = auth_context

    def get_cli_version(self):
        return '0.1.0'


sfbcli = SfbCLI(cli_name=cli_name,
              config_dir=CONFIG_DIR,
              config_env_var_prefix=cli_name,
              commands_loader_cls=SfbCLICommandsLoader,
              help_cls=SfbCLIHelp)

sfbcli.register_event(EVENT_CLI_PRE_EXECUTE, eh.pre_execute_load_config_handler)

exit_code = sfbcli.invoke(sys.argv[1:])
sys.exit(exit_code)
